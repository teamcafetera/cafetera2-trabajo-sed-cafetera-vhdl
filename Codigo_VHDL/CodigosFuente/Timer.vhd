----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.12.2019 11:48:06
-- Design Name: 
-- Module Name: Timer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity Timer is
generic(
    NBit:positive:=5
    );
  Port ( 
  Load: in std_logic;
  Tiempo:in std_logic_vector( NBit - 1 downto 0);
  Clk: in std_logic;
  Reset: in std_logic;
  Seconds:out std_logic_vector( NBit - 1 downto 0);
  Ready:out std_logic--Se activa cuando la cuenta llega a un determinado numero de segundos
  ); 
end Timer;

architecture behavioral of Timer is
  signal count: unsigned(Tiempo'range);
  signal Seconds_i:unsigned(Seconds'range);
begin
  process (Reset,Clk,Load)
  begin
    if Reset = '1' then
      count <= (others => '0');
      Seconds_i <= (others => '0');
    elsif  Load = '1' and count = 0 then
        count <= unsigned(Tiempo);
        Seconds_i <= (others => '0');
    elsif Clk'event and Clk = '1' then
        if count /= 0 then
            count <= count - 1;
            Seconds_i <= Seconds_i + 1;
        end if;
    end if;
  end process;

  Ready <= '1' when count = 0 else
       '0';
  Seconds<=std_logic_vector(Seconds_i);
end behavioral;




--architecture Behavioral of Timer is
--type state_type IS (S0, S1,S2);-- S0 inicial S1 contando S2 ready
--signal state, next_state: state_type;
--signal Seconds_i: integer range 0 to TMax;
--signal Ready_i:std_logic;
--begin
--    SYNC_PROC: PROCESS (Clk,state)
--    BEGIN
--        IF rising_edge(clk) THEN
--            IF Reset = '1' THEN
--                state <= S0;  
--            ELSE
--                state <= next_state;
               
--            END IF;
--        END IF;
--    END PROCESS;
    
    
--    OUTPUT_DECODE: PROCESS (Clk,state)
--    BEGIN
--        CASE (state) is
--             WHEN S0 => 
--                  Seconds_i<=0;
--                  Ready_i <= '0';
--             WHEN S1 => 
--                IF rising_edge(clk)  THEN
--                    Seconds_i<=Seconds_i + 1;
--                end if;
--              WHEN S2 =>
--                Ready_i<='1';
--             WHEN OTHERS => Ready_i<='U';
--        END CASE;
--    END PROCESS;


--    NEXT_STATE_DECODE: PROCESS (Seconds_i,state, Load)
--    BEGIN
--        CASE (state) is
--            WHEN S0 =>
--                IF Load = '1' THEN
--                    next_state <= S1;
--                END IF;
--            WHEN S1 =>
--               IF Seconds_i = Tiempo -1 THEN
--                   next_state <= S2;
--               END IF;
--            WHEN S2 => 
--                next_state <= S0;
--           END CASE;
--     END PROCESS; 
-- Seconds<=Seconds_i;
-- Ready<=Ready_i;
--end Behavioral;
