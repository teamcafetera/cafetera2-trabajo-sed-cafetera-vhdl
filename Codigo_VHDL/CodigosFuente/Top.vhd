----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.12.2019 11:39:10
-- Design Name: 
-- Module Name: Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top is
    generic(    
        NBit:positive:=5;
        NLed:positive:=10;
        NOpc:positive:=2; --Numero de opciones del flicker
        N_BitOpc_Displ:positive:=3 
         );
    Port ( 
         RESET: in std_logic;
         START: in std_logic;
         BOTON_CORTO: in std_logic;
         BOTON_LARGO: in std_logic;
         SENSOR_AGUA: in std_logic;
         SENSOR_VASO: in std_logic;
         SENSOR_CAPSULA: in std_logic;
         FAST_CLK: in std_logic;
         LED_CAFETERA_ENCENDIDA: OUT std_logic;
         LED_RESISTENCIA_AGUA: OUT std_logic;
         LED_NO_CAPSULA: OUT std_logic;
         LED_NO_VASO: OUT std_logic;
         LED_ELECCION_CAFE: OUT std_logic;
         LED_CAFE_LISTO: OUT std_logic;
         LED_DISPENSANDO_CAFE : out STD_LOGIC_VECTOR(9 downto 0);
         display_number:out STD_LOGIC_VECTOR(6 downto 0);
         display_selection:out STD_LOGIC_VECTOR(7 downto 0)
  );
end Top;

architecture Behavioral of Top is
  signal load_timer_i:std_logic;
  signal seconds_i : std_logic_vector( NBit - 1 downto 0);
  signal tiempo_i : std_logic_vector( NBit - 1 downto 0);
  signal ready_timer_i:std_logic;
  signal clock_1Hz_i:std_logic;
  signal clock_100Hz_i:std_logic;
  signal enable_decoder_i:std_logic;
  signal enable_flicker_i:std_logic;
  signal opcion_flicker_i : std_logic_vector (NOpc - 1 downto 0);
  signal enable_displays_i:std_logic;
  signal opcion_displays_i : std_logic_vector (N_BitOpc_Displ - 1 downto 0);
  signal segment_display_0_i : std_logic_vector(6 downto 0);
  signal segment_display_1_i : std_logic_vector(6 downto 0);
  signal segment_display_2_i : std_logic_vector(6 downto 0);
  signal segment_display_3_i : std_logic_vector(6 downto 0);
  signal segment_display_4_i : std_logic_vector(6 downto 0);
  signal segment_display_5_i : std_logic_vector(6 downto 0);
  signal segment_display_6_i : std_logic_vector(6 downto 0);
  signal segment_display_7_i : std_logic_vector(6 downto 0);
  
  COMPONENT CLK_Divider
        PORT (
                    Reset: in std_logic;
                    CLK_in: in std_logic;
                    CLK_out:out std_logic
         );
   END COMPONENT;
         
  COMPONENT Clk_Divider_2
        PORT (
                    reset: in std_logic;
                    clk_in: in std_logic;
                    clk_out:out std_logic
         );
    END COMPONENT;
    
    
    COMPONENT Decoder
        generic(
                NBit:positive:=5;
                NLed:positive:=10
                );
        Port ( 
                Enable: in std_logic;
                Numero: in std_logic_vector( NBit - 1 downto 0);
                Led : out std_logic_vector(NLed-1 downto 0)
            );
    END COMPONENT;
    
    
     COMPONENT Flicker
         generic(
                NOpc:positive:=2 --Numero de opciones
              );
        Port ( 
                 Enable: in std_logic;
                 Clk: in std_logic;
                 Opcion: in std_logic_vector (NOpc - 1 downto 0);
                 Led : out std_logic
            );
    END COMPONENT;
    
    
    COMPONENT Timer
        generic(
                 NBit:positive:=5
         );
        Port ( 
                Load: in std_logic;
                Tiempo:in std_logic_vector( NBit - 1 downto 0);
                Clk: in std_logic;
                Reset: in std_logic;
                Seconds:out std_logic_vector( NBit - 1 downto 0);
                Ready:out std_logic--Se activa cuando la cuenta llega a un determinado numero de segundos
            ); 
    END COMPONENT;
  
  
  COMPONENT Fsm
        generic(
            NBit_Tiempo:positive:=5;
            NOpc_Flick:positive:=2; --Numero de opciones de flicker
            N_BitOpc_Displ:positive:=3 --Numero de bits para las 5 opciones de los displays
            );
    
        Port ( 
            Reset: in std_logic;
            Clk: in std_logic;
            Start: in std_logic;
            Timer_Ready: in std_logic;
            Boton_Corto: in std_logic;
            Boton_Largo: in std_logic;
            Sensor_Agua: in std_logic; -- Nivel alto si hay agua
            Sensor_Vaso: in std_logic;
            Sensor_Capsula: in std_logic;
            Led_Cafetera_Encendida: out std_logic;
            Led_No_Capsula: out std_logic; --Encendido si no esta metida la capsula
            Led_No_Vaso: out std_logic;--Encendido si no esta puesto el vaso
            Led_Eleccion_Cafe: out std_logic; ---Se enciende este led cuando la cafetera esta lista para empezar a echar cafe
            Led_Cafe_Listo: out std_logic;
            Enable_Decoder: out std_logic;
            Enable_Flicker: out std_logic;
            Enable_Displays: out std_logic;
            Load_Timer: out std_logic;
            Tiempo:out std_logic_vector( NBit_Tiempo - 1 downto 0);
            Opcion_Flicker: out std_logic_vector (NOpc_Flick - 1 downto 0);
            Opcion_Displays: out std_logic_vector (N_BitOpc_Displ - 1 downto 0)
               --Estado: out integer range 0 to 11 --Esta variable se ha usado para controlar en que estado se encontraba la maquina y depurar codigo
            );
    END COMPONENT;
    
    COMPONENT Decoder_Displays
           generic(
          NBitOpc:positive:=3 --El Numero de opciones son 5 por tanto vector de 3 bits sirve
            );
          Port (
                Enable: in std_logic;
                Opcion: in std_logic_vector (NBitOpc - 1 downto 0);
                segment_display_0 : OUT std_logic_vector(6 downto 0);
		        segment_display_1 : OUT std_logic_vector(6 downto 0);
		        segment_display_2 : OUT std_logic_vector(6 downto 0);
		        segment_display_3 : OUT std_logic_vector(6 downto 0);
		        segment_display_4 : OUT std_logic_vector(6 downto 0);
		        segment_display_5 : OUT std_logic_vector(6 downto 0);
		        segment_display_6 : OUT std_logic_vector(6 downto 0);
		        segment_display_7 : OUT std_logic_vector(6 downto 0)
            );
    END COMPONENT;
    
    COMPONENT Display_Refresh
           Port ( 
		  clk_in : in  STD_LOGIC;
          segment_display_0 : IN std_logic_vector(6 downto 0);
		  segment_display_1 : IN std_logic_vector(6 downto 0);
		  segment_display_2 : IN std_logic_vector(6 downto 0);
		  segment_display_3 : IN std_logic_vector(6 downto 0);
		  segment_display_4 : IN std_logic_vector(6 downto 0);
		  segment_display_5 : IN std_logic_vector(6 downto 0);
		  segment_display_6 : IN std_logic_vector(6 downto 0);
		  segment_display_7 : IN std_logic_vector(6 downto 0);   
          display_number : out  STD_LOGIC_VECTOR (6 downto 0);
          display_selection : out  STD_LOGIC_VECTOR (7 downto 0)
        );
    END COMPONENT;
 
begin

        Inst_CLK_Divider: CLK_Divider PORT MAP (
        Reset => RESET,
        CLK_in=> FAST_CLK,
        CLK_out=>clock_1HZ_i
        );
        
         Inst_CLK_Divider_2: CLK_Divider_2 PORT MAP (
        Reset => RESET,
        CLK_in=> FAST_CLK,
        CLK_out=>clock_100HZ_i
        );

        Inst_Decoder: Decoder PORT MAP (
        Enable => enable_decoder_i,
        Numero=> seconds_i,
        Led=>LED_DISPENSANDO_CAFE
        );
        
         Inst_Flicker: Flicker PORT MAP (
        Enable => enable_flicker_i,
        Clk =>clock_1HZ_i,
        Opcion=> opcion_flicker_i,
        Led=>LED_RESISTENCIA_AGUA
        );
        
        Inst_Timer: Timer PORT MAP (
        Load => load_timer_i,
        Tiempo=> tiempo_i,
        Clk=>clock_1HZ_i,
        Reset=>RESET,
        Seconds=>Seconds_i,
        Ready=>ready_timer_i
        );
        
        Inst_Fsm: Fsm PORT MAP (
        Reset => RESET,
        Clk => FAST_CLK,
        Start => START, 
        Timer_Ready => ready_timer_i,
        Boton_Corto => BOTON_CORTO,
        Boton_Largo => BOTON_LARGO,
        Sensor_Agua => SENSOR_AGUA, 
        Sensor_Vaso => SENSOR_VASO,
        Sensor_Capsula => SENSOR_CAPSULA,
        Led_Cafetera_Encendida => LED_CAFETERA_ENCENDIDA,
        Led_No_Capsula => LED_NO_CAPSULA,
        Led_No_Vaso => LED_NO_VASO,
        Led_Eleccion_Cafe => LED_ELECCION_CAFE,
        Led_Cafe_Listo => LED_CAFE_LISTO,
        Enable_Decoder => enable_decoder_i,
        Enable_Flicker => enable_flicker_i,
        Load_Timer => load_timer_i,
        Tiempo => tiempo_i,
        Opcion_Flicker => opcion_flicker_i,
        Enable_Displays => enable_displays_i,
        Opcion_Displays => opcion_displays_i
        );
        
        Inst_Decoder_Displays: Decoder_Displays PORT MAP (
        Enable => enable_displays_i,
        Opcion=> opcion_displays_i,
        segment_display_0 =>segment_display_0_i,
        segment_display_1 =>segment_display_1_i,
        segment_display_2 =>segment_display_2_i,
        segment_display_3 =>segment_display_3_i,
        segment_display_4 =>segment_display_4_i,
        segment_display_5 =>segment_display_5_i,
        segment_display_6 =>segment_display_6_i,
        segment_display_7 =>segment_display_7_i
        );
        
        Inst_Display_Refresh: Display_Refresh PORT MAP (
        clk_in => clock_100HZ_i,
        segment_display_0 =>segment_display_0_i,
        segment_display_1 =>segment_display_1_i,
        segment_display_2 =>segment_display_2_i,
        segment_display_3 =>segment_display_3_i,
        segment_display_4 =>segment_display_4_i,
        segment_display_5 =>segment_display_5_i,
        segment_display_6 =>segment_display_6_i,
        segment_display_7 =>segment_display_7_i,
        display_number=>display_number,
        display_selection=>display_selection
        );
        

end Behavioral;
