----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2020 15:48:28
-- Design Name: 
-- Module Name: Display_Refresh - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;




entity display_refresh is
    Port ( 
		  clk_in : in  STD_LOGIC;
          segment_display_0 : IN std_logic_vector(6 downto 0);
		  segment_display_1 : IN std_logic_vector(6 downto 0);
		  segment_display_2 : IN std_logic_vector(6 downto 0);
		  segment_display_3 : IN std_logic_vector(6 downto 0);
		  segment_display_4 : IN std_logic_vector(6 downto 0);
		  segment_display_5 : IN std_logic_vector(6 downto 0);
		  segment_display_6 : IN std_logic_vector(6 downto 0);
		  segment_display_7 : IN std_logic_vector(6 downto 0);   
          display_number : out  STD_LOGIC_VECTOR (6 downto 0);
          display_selection : out  STD_LOGIC_VECTOR (7 downto 0)
        );
end display_refresh;

architecture Behavioral of display_refresh is

begin

	muestra_displays:process (clk_in, segment_display_0, segment_display_1, segment_display_2, segment_display_3 )
	variable cnt:integer range 0 to 7;
	begin
		if (clk_in'event and clk_in='1') then 
			if cnt=7 then
				cnt:=0;
			else
				cnt:=cnt+1;
			end if;
		end if;
		
		case cnt is
				when 0 => display_selection<="11111110";
						    display_number<=segment_display_0;
				
				when 1 => display_selection<="11111101";
						    display_number<=segment_display_1;
				
				when 2 => display_selection<="11111011";
						    display_number<=segment_display_2;
				
				when 3 => display_selection<="11110111";
						    display_number<=segment_display_3;
						    
			    when 4 => display_selection<="11101111";
						    display_number<=segment_display_4;	
						    
			    when 5 => display_selection<="11011111";
						    display_number<=segment_display_5;
						    
			    when 6 => display_selection<="10111111";
						    display_number<=segment_display_6;
						    
			    when 7 => display_selection<="01111111";
						    display_number<=segment_display_7;			
			end case;

	end process;

end Behavioral;
