----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2020 15:53:13
-- Design Name: 
-- Module Name: Outs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decoder_Displays is
  generic(
          NBitOpc:positive:=3 --El Numero de opciones son 5 por tanto vector de 3 bits sirve
  );
 Port (
          Enable: in std_logic;
          Opcion: in std_logic_vector (NBitOpc - 1 downto 0);
          segment_display_0 : OUT std_logic_vector(6 downto 0);
		  segment_display_1 : OUT std_logic_vector(6 downto 0);
		  segment_display_2 : OUT std_logic_vector(6 downto 0);
		  segment_display_3 : OUT std_logic_vector(6 downto 0);
		  segment_display_4 : OUT std_logic_vector(6 downto 0);
		  segment_display_5 : OUT std_logic_vector(6 downto 0);
		  segment_display_6 : OUT std_logic_vector(6 downto 0);
		  segment_display_7 : OUT std_logic_vector(6 downto 0)
		  
 );
end Decoder_Displays;

architecture Behavioral of Decoder_Displays is
begin

    Elige_opcion:process (Enable,Opcion)
	begin
	   if Enable='1' then
        case (to_integer(unsigned(Opcion))) is
				when 1 => --Escribimos NO AGUA
				    --Escribimos nada
                    segment_display_7<="1111111";
                    --Escribimos N
                    segment_display_6<="0001001";
                    --Escribimos O
                    segment_display_5<="0000001";
                    --Escribimos nada
                    segment_display_4<="1111111";
                    --Escribimos A
                    segment_display_3<="0001000";
                    --Escribimos G
                    segment_display_2<="0100000";
                    --Escribimos U
                    segment_display_1<="1000001";
                    --Escribimos A
                    segment_display_0<="0001000";
                  
                when 2 => --Escribimos  NO CAPSUL
				    --Escribimos N
                    segment_display_7<="0001001";
                    --Escribimos O
                    segment_display_6<="0000001";
                    --Escribimos C
                    segment_display_5<="0110001";
                    --Escribimos A
                    segment_display_4<="0001000";
                    --Escribimos P
                    segment_display_3<="0011000";
                    --Escribimos S
                    segment_display_2<="0100100";
                    --Escribimos U
                    segment_display_1<="1000001";
                    --Escribimos L
                    segment_display_0<="1110001";
               
              when 3 =>--Escribimos NO VASO 
				    --Escribimos nada
                    segment_display_7<="1111111";
                    --Escribimos N
                    segment_display_6<="0001001";
                    --Escribimos O
                    segment_display_5<="0000001";
                    --Escribimos nada
                    segment_display_4<="1111111";
                    --Escribimos V es igual que la U
                    segment_display_3<="1000001";
                    --Escribimos A
                    segment_display_2<="0001000";
                    --Escribimos S
                    segment_display_1<="0100100";
                    --Escribimos O
                    segment_display_0<="0000001";
                    
               when 4 =>--Escribimos ELIGE 
				    --Escribimos nada
                    segment_display_7<="1111111";
                    --Escribimos nada
                    segment_display_6<="1111111";
                    --Escribimos nada
                    segment_display_5<="1111111";
                    --Escribimos E
                    segment_display_4<="0110000";
                    --Escribimos L
                    segment_display_3<="1110001";
                    --Escribimos I
                    segment_display_2<="1001111";
                    --Escribimos G
                    segment_display_1<="0100000";
                    --Escribimos E
                    segment_display_0<="0110000";
                    
                when 5 =>--Escribimos LISTO 
				    --Escribimos nada
                    segment_display_7<="1111111";
                    --Escribimos nada
                    segment_display_6<="1111111";
                    --Escribimos nada
                    segment_display_5<="1111111";
                    --Escribimos L
                    segment_display_4<="1110001";
                    --Escribimos I
                    segment_display_3<="1001111";
                    --Escribimos S
                    segment_display_2<="0100100";
                    --Escribimos T
                    segment_display_1<="0001111";
                    --Escribimos O
                    segment_display_0<="0000001";
                   
                 when others => -- Escribimos nada   
                    segment_display_7<="1111111";
                    segment_display_6<="1111111";
                    segment_display_5<="1111111";
                    segment_display_4<="1111111";
                    segment_display_3<="1111111";
                    segment_display_2<="1111111";
                    segment_display_1<="1111111";
                    segment_display_0<="1111111";
                    
                    
             end case;
         else
            segment_display_7<="1111111";
            segment_display_6<="1111111";
            segment_display_5<="1111111";
            segment_display_4<="1111111";
            segment_display_3<="1111111";
            segment_display_2<="1111111";
            segment_display_1<="1111111";
            segment_display_0<="1111111";
         end if;
      end process;              

            
end Behavioral;
