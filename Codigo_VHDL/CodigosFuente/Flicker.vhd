----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.01.2020 18:07:48
-- Design Name: 
-- Module Name: Flicker - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Flicker is
 generic(
 NOpc:positive:=2 --Numero de opciones
 );
 Port ( 
        Enable: in std_logic;
        Clk: in std_logic;
        Opcion: in std_logic_vector (NOpc - 1 downto 0);
        Led : out std_logic
 );
end Flicker;

architecture behavioral of Flicker is
begin
    process(Enable,Opcion,Clk)  
    begin
        if Enable = '1' then 
            case Opcion is
             when "01" => Led<='1';
             
             when "10" => 
               if Clk='0' then
                    Led<='0';
               elsif Clk='1' then
                    Led<='1';
               end if;
                  
             when others => Led<='0';   
            end case; 
        else 
            Led<='0';
        END IF;
      end process;       
   
end behavioral;