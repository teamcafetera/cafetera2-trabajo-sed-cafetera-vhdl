----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2020 19:06:18
-- Design Name: 
-- Module Name: Fsm_Prueba_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Fsm_tb is
end Fsm_tb;

architecture behavior of Fsm_tb is 

  constant ClockPeriod      : time := 10 ns; --Ajustar este parametro a nuestro periodo
  constant NBit_Tiempo:positive:=5;
  constant NOpc_Flick:positive:=2; 
  constant N_BitOpc_Displ:positive:=3;
  signal Reset:  std_logic;
  signal Clk:  std_logic;
  signal Start: std_logic;
  signal Timer_Ready: std_logic;
  signal Boton_Corto: std_logic;
  signal Boton_Largo: std_logic;
  signal Sensor_Agua: std_logic; -- Nivel alto si hay agua
  signal Sensor_Vaso: std_logic;
  signal Sensor_Capsula: std_logic;
  signal Led_Cafetera_Encendida: std_logic;
  signal Led_No_Capsula: std_logic;
  signal Led_No_Vaso: std_logic;
  signal Led_Eleccion_Cafe: std_logic; ---Se enciende este led cuando la cafetera esta lista para empezar a echar cafe
  signal Led_Cafe_Listo: std_logic;
  signal Enable_Decoder: std_logic;
  signal Enable_Flicker: std_logic;
  signal Enable_Displays: std_logic;
  signal Load_Timer: std_logic;
  signal Tiempo: std_logic_vector( NBit_Tiempo - 1 downto 0);
  signal Opcion_Flicker:  std_logic_vector (NOpc_Flick - 1 downto 0);
  signal Opcion_Displays: std_logic_vector (N_BitOpc_Displ - 1 downto 0);
 -- signal Estado: integer range 0 to 11;
  
    component Fsm is
        generic(
                NBit_Tiempo:positive:=5;
                NOpc_Flick:positive:=2; --Numero de opciones de flicker
                N_BitOpc_Displ:positive:=3 --Numero de bits para las 5 opciones de los displays
    );
        port (
                Reset: in std_logic;
                Clk: in std_logic;
                Start: in std_logic;
                Timer_Ready: in std_logic;
                Boton_Corto: in std_logic;
                Boton_Largo: in std_logic;
                Sensor_Agua: in std_logic; -- Nivel alto si hay agua
                Sensor_Vaso: in std_logic;
                Sensor_Capsula: in std_logic;
                Led_Cafetera_Encendida: out std_logic;
                Led_No_Capsula: out std_logic; --Encendido si no esta metida la capsula
                Led_No_Vaso: out std_logic;--Encendido si no esta puesto el vaso
                Led_Eleccion_Cafe: out std_logic; ---Se enciende este led cuando la cafetera esta lista para empezar a echar cafe
                Led_Cafe_Listo: out std_logic;
                Enable_Decoder: out std_logic;
                Enable_Flicker: out std_logic;
                Enable_Displays: out std_logic;
                Load_Timer: out std_logic;
                Tiempo:out std_logic_vector( NBit_Tiempo - 1 downto 0);
                Opcion_Flicker: out std_logic_vector (NOpc_Flick - 1 downto 0);
                Opcion_Displays: out std_logic_vector (N_BitOpc_Displ - 1 downto 0)
              --  Estado: out integer range 0 to 11 --Esta variable la usamos para depurar codigo
             );
    end component;
begin
    uut: Fsm port map (
        Reset => Reset,
        Clk => Clk,
        Start => Start,
        Timer_Ready => Timer_Ready,
        Boton_Corto => Boton_Corto,
        Boton_Largo => Boton_Largo,
        Sensor_Agua=>Sensor_Agua,
        Sensor_Vaso=>Sensor_Vaso,
        Sensor_Capsula=>Sensor_Capsula,
        Led_Cafetera_Encendida=>Led_Cafetera_Encendida,
        Led_No_Capsula=>Led_No_Capsula,
        Led_No_Vaso=>Led_No_Vaso,
        Led_Eleccion_Cafe=>Led_Eleccion_Cafe,
        Led_Cafe_Listo => Led_Cafe_Listo,
        Enable_Decoder=>Enable_Decoder,
        Enable_Flicker=>Enable_Flicker,
        Enable_Displays=>Enable_Displays,
        Load_Timer=>Load_Timer,
        Tiempo=>Tiempo,
        Opcion_Flicker=>Opcion_Flicker,
        Opcion_Displays=>Opcion_Displays
      --  Estado=>Estado
    );
    
   p0: process
    begin
    Clk <= '0';
    wait for 0.5 * ClockPeriod;
    Clk<= '1';
    wait for 0.5 * ClockPeriod;
    end process;
    
    p1:process
    begin
      Reset <= '1', '0' after 0.25 * ClockPeriod;
      
      --Encendemos la cafetera
       wait for 2*ClockPeriod;
       Start <= '1';
       --Introducimos el agua
       wait for 20*ClockPeriod;
       Sensor_Agua <= '1';
       
       --Simulacion pulso de temporizador ha filazado
       Timer_Ready <= '0';
       wait for 30*ClockPeriod;--Simulamos como 30 segundos calentando agua
       Timer_Ready <= '1';
       wait for 10*ClockPeriod;
       Timer_Ready <= '0';
       
       --Introducimos la capsula
       wait for 5*ClockPeriod;
       Sensor_Capsula <= '1';
       
       
--       Comprobacion a apagarla para observar que todas las salidas se apagan
--        Start <= '0';
       
       --Colocamos el vaso
       wait for 5*ClockPeriod;
       Sensor_Vaso <= '1';
       
       
       --Simulacion de pulsar el boton
       wait for 20*ClockPeriod;
       Boton_Largo <= '1';  -- Boton_Corto <= '1';
       wait for 2*ClockPeriod;
       Boton_Largo <= '0';  -- Boton_Corto <= '0';
       
       
        --Simulacion pulso de temporizador ha filazado
       Timer_Ready <= '0';
       wait for 20*ClockPeriod;--Simulamos como 20 segundos calentando agua
       Timer_Ready <= '1';
       wait for ClockPeriod;
       Timer_Ready <= '0';
       
       
       --Cuando el cafe esta listo quitamos el vaso
       wait for 2*ClockPeriod;
        Sensor_Vaso<= '0';
       
--       Este es el caso de que siga quedando agua
       wait for 20*ClockPeriod;
        Sensor_Capsula<= '0';
        wait for 20*ClockPeriod;
        Sensor_Capsula<= '1';
        
      --Este es el caso de que no quede agua
--        Sensor_Agua<= '0';

      wait;
   end process;
    
end;


