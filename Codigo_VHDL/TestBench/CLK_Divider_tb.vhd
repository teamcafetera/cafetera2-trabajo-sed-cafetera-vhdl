----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.12.2019 20:03:49
-- Design Name: 
-- Module Name: CLK_Divider_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CLK_Divider_tb is
end CLK_Divider_tb;

architecture Behavioral of CLK_Divider_tb is
    signal Reset: std_logic;
    signal CLK_in: std_logic;
    signal CLK_out: std_logic;

    component CLK_divider is
    port (
        Reset: in std_logic;
        CLK_in: in std_logic;
        CLK_out: out std_logic
        );
    end component;    
    
    constant k: time := 10 ns;
begin
    uut: CLK_Divider port map (Reset, CLK_in,CLK_out);
    
    Reset <= '1' after 0.25 * k, '0' after 0.75 * k;
    
    p0: process
    begin
    CLK_in <= '0';
    wait for 0.5 * k;
    CLK_in<= '1';
    wait for 0.5 * k;
    end process;
     
    p1: process
    begin
    wait;
    end process;



end Behavioral;
