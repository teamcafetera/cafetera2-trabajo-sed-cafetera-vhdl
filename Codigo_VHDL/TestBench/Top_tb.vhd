----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.12.2019 12:36:29
-- Design Name: 
-- Module Name: Top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top_tb is
end Top_tb;

architecture Behavioral of Top_tb is

  signal RESET: std_logic;
  signal START: std_logic;
  signal BOTON_CORTO: std_logic;
  signal BOTON_LARGO: std_logic;
  signal SENSOR_AGUA: std_logic;
  signal SENSOR_VASO: std_logic;
  signal SENSOR_CAPSULA: std_logic;
  signal FAST_CLK: std_logic;
  signal  LED_CAFETERA_ENCENDIDA: std_logic;
  signal LED_RESISTENCIA_AGUA: std_logic;
  signal LED_NO_CAPSULA: std_logic;
  signal LED_NO_VASO: std_logic;
  signal LED_ELECCION_CAFE: std_logic;
  signal LED_CAFE_LISTO: std_logic;
  signal  LED_DISPENSANDO_CAFE : STD_LOGIC_VECTOR(9 downto 0);
  constant ClockPeriod      : time := 10 ns;
  constant seg: time := 1 sec;
  component Top is
        port (
                  RESET: in std_logic;
                  START: in std_logic;
                  BOTON_CORTO: in std_logic;
                  BOTON_LARGO: in std_logic;
                  SENSOR_AGUA: in std_logic;
                  SENSOR_VASO: in std_logic;
                  SENSOR_CAPSULA: in std_logic;
                  FAST_CLK: in std_logic;
                  LED_CAFETERA_ENCENDIDA: OUT std_logic;
                  LED_RESISTENCIA_AGUA: OUT std_logic;
                  LED_NO_CAPSULA: OUT std_logic;
                  LED_NO_VASO: OUT std_logic;
                  LED_ELECCION_CAFE: OUT std_logic;
                  LED_CAFE_LISTO: OUT std_logic;
                  LED_DISPENSANDO_CAFE : out STD_LOGIC_VECTOR(9 downto 0)
             );
    end component;
begin
uut: Top port map (
        RESET => RESET,
        START => START,
        BOTON_CORTO => BOTON_CORTO,
        BOTON_LARGO => BOTON_LARGO,
        SENSOR_AGUA => SENSOR_AGUA,
        SENSOR_VASO => SENSOR_VASO,
        SENSOR_CAPSULA => SENSOR_CAPSULA,
        FAST_CLK => FAST_CLK,
        LED_CAFETERA_ENCENDIDA => LED_CAFETERA_ENCENDIDA,
        LED_RESISTENCIA_AGUA => LED_RESISTENCIA_AGUA,
        LED_NO_CAPSULA => LED_NO_CAPSULA,
        LED_NO_VASO => LED_NO_VASO,
        LED_ELECCION_CAFE => LED_ELECCION_CAFE,
        LED_CAFE_LISTO => LED_CAFE_LISTO,
        LED_DISPENSANDO_CAFE => LED_DISPENSANDO_CAFE
    );
    
    
    p0: process
    begin
         FAST_CLK <= '0';
         wait for 0.5 * ClockPeriod;
         FAST_CLK<= '1';
          wait for 0.5 * ClockPeriod;
    end process;
    
    p1: process
    begin
     RESET<= '1', '0' after 0.25 * ClockPeriod;
     wait for 5 * ClockPeriod;
     START<= '1';
     wait for 5 * ClockPeriod;
     SENSOR_AGUA<= '1';
     wait for 3.75 * seg;
     SENSOR_CAPSULA<= '1';
     wait for 0.5 * seg;
     SENSOR_VASO<= '1';
     wait for 0.5 * seg;
     BOTON_CORTO<= '1';
     wait for 4 * seg;
     SENSOR_AGUA<= '0';
     wait;
    end process;
end Behavioral;

