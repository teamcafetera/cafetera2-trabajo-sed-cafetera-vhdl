

# CAFETERA DE CÁPSULAS PARA USO DIARIO

## AUTORES
Alex Bret de Sivatte, Jaime González Plaza y Javier Gómez Alonso.


## FUNCIONAMIENTO
Hemos realizado el diseño con lógica programable de una cafetera de uso doméstico. Su funcionamiento resumidamente es
el siguiente.
En primer lugar, el usuario pulsa el botón de Start para encenderla. A continuación, introduce el agua, la cápsula y 
coloca el vaso en el lugar correspondiente.
En este momento, la cafetera le indica que elija un café corto o uno largo. Al pulsar una opción se enciende una tira de
leds acorde con la dispensación del café.
Cuando termina el proceso la cafetera te indica que el café esta listo.
Transcurrido estos pasos si queda agua el usuario cambiaría la cápsula y realizaría otro cafe, y así sucesivamente. Por el
contrario si el agua esta agotada se recargará, se introducirá la capsula y el vaso, continuando con el mismo proceso de la
elección del tipo de café anteriormente nombrado.


## DISTRIBUCIÓN EN LA FPGA

![Representacion de pines utilizados en la tarjeta](images/Tarjeta.jpg)

   
## CONEXIÓN DE LOS MODULOS EN TOP

![Representacion de pines utilizados en la tarjeta](images/Modulos/Top.png)

   
## VÍDEO FUNCIONAMIENTO CAFETERA

[Cafetera de cápsulas para uso diario](https://youtu.be/3aO__Cb5D9s )















